import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }
  warriors = [];
  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  handleFighterClick = async (event, fighter) => {
    if(!this.fightersDetailsMap.has(fighter._id)) {
      this.fightersDetailsMap.set(fighter._id, await fighterService.getFighterDetails(fighter._id));
      console.log(this.fightersDetailsMap);
    } else {
      console.log(this.fightersDetailsMap);
    }
    this.warriors.push(fighter);
    if(this.warriors.length > 1) {
      this.fight(this.warriors[0], this.warriors[1])
    }
    const info = document.getElementById("info");
    const detailsField = document.getElementById("info-content");
    let details = this.fightersDetailsMap.get(fighter._id);
    const detailsElements = {
      name: this.createElement({tagName: "h3", className: "name",attributes: {
        innerText: details.name,
        id: "name"
      }}),
      health: this.createElement({tagName: "input", className: "health", attributes: {
        value: details.health,
        id: "health"
      }}),
      attack: this.createElement({tagName: "input", className: "attack", attributes: {
        value: details.attack,
        id: "attack"
      }}),
      defence: this.createElement({tagName: "input", className: "defense", attributes: {
        value: details.defense,
        id: "defense"
      }})
    };
    for (let node of Object.values(detailsElements)) {
      // let label = this.createElement({tagName: "label", className: "", attributes: {
      //   for: node.id
      // }});
      // detailsField.appendChild(label);
      detailsField.appendChild(node);
    }
    info.style.display = "block";
  }

  fight(fighter1, fighter2) {
    const root = document.getElementById('root');
    this.fighter1 = fighter1;
    this.fighter2 = fighter2;
    const fighter1Details = this.fightersDetailsMap.get(fighter1._id);
    const fighter2Details = this.fightersDetailsMap.get(fighter2._id);
    this.fighter1.health = fighter1Details.health - this.getHitPower(fighter2Details.attack) + this.getBlockPower(fighter1Details.defense);
    this.fighter2.health = fighter2Details.health - this.getHitPower(fighter1Details.attack) + this.getBlockPower(fighter2Details.defense);
    if(fighter1Details.health <= 45 || fighter2Details.health <= 45) {
       root.innerText = fighter1Details.health <= 45 ? `${fighter2.name} Won!` : `${fighter2.name} Won!`;
    } else {
      this.fight(this.fighter1, this.fighter2);
    }
  }

  getHitPower(attack) {
    let criticalHitChance = Math.floor(Math.random()*2)+1;
    return attack*criticalHitChance;
  }

  getBlockPower(defense) {
    let dodgeChance = Math.floor(Math.random()*2)+1;
    return defense * dodgeChance
  }
}

export default FightersView;